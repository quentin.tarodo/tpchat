import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitreComponent } from './titre/titre.component';
import { MenuComponent } from './menu/menu.component';
import { MessagesComponent } from './messages/messages.component';
import { InputChatComponent } from './input-chat/input-chat.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { ChannelComponent } from './channel/channel.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    AppComponent,
    TitreComponent,
    MenuComponent,
    MessagesComponent,
    InputChatComponent,
    LoginComponent,
    ChannelComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
