import { Component, OnInit, Input } from '@angular/core';
import { Channel } from 'src/channel';
import { ParamMap, ActivatedRoute } from '@angular/router';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {
  channel: Channel

  constructor(private activatedRoute: ActivatedRoute, private chatService: ChatService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.chatService.getCurrentChannel(params.get('id')).subscribe(
        (channel: Channel) => this.channel = channel
              )
      })
  }



}
