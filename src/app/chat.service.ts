import { Injectable } from '@angular/core';
import { Channel } from 'src/Channel';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Message } from 'src/message';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
currentChannel: Observable<Channel>
pseudoLoged: string
messages: string[]
  constructor(private http: HttpClient) { }

refreshMessages(){

}
sendMessage(){

}

getChannels(): Observable<Channel[]> {
  return this.http.get<Channel[]>('http://localhost:3000/channels')
  }

getCurrentChannel(id: string): Observable<Channel> {
    this.currentChannel = this.http.get<Channel>('http://localhost:3000/channels/'+id)
    return this.currentChannel
    }

addMessage(channel: Channel, message: Message): Observable<Message> {
      return this.http.post<Message>('http://localhost:3000/channels/'+channel.id+'/messages', message)
}

getMessages(): Observable<Message[]> {
  return this.http.get<Message[]>('http://localhost:3000/messages')
}

}
