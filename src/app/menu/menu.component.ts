import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';
import { Channel } from 'src/channel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  channels: Channel[]
  constructor(private chatService: ChatService,
     private router: Router) { }

  ngOnInit() {
    this.chatService.getChannels().subscribe(
      (channels: Channel[]) => this.channels = channels
    )
  }

 
   

}
