import { Component, OnInit } from '@angular/core';
import { Message } from 'src/message';
import { ChatService } from '../chat.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Channel } from 'src/channel';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  channel: Channel
  messages: Message[]

  constructor(private chatService: ChatService, private activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      this.chatService.getCurrentChannel(params.get('id')).subscribe(
        (channel: Channel) => this.channel = channel
              )
      })
    this.chatService.getMessages().subscribe(
      
      (messages: Message[]) => this.messages = messages.filter(m => m.idSalon !== this.channel.id)  
      
    )
  }

  
}
